Izaak Miller
March 13th, 2017

Questions:

1:
i8042 -  Is the keyboard
rtc0 - The real time clock
acpi - Advanced configuration and power interface
ehci_hcd:usb1 - Enhanced Host Controller Interface, enables a host controller
for a usb port
ehci_hcd:usb2 - Enhanced Host Controller Interface, enables a host controller
for a usb port

2:
The most common interrupts tended to be the hardware interacting with the
system such as the keyboard, mouser, system timer and clock. These seemed to
have frequent interrupts that appeared more often than other things.

3:
cpu0        cpu1      cpu2       cpu3
8710222    8703846    9871002    6293364   Local timer interrupts

4:
The local timer interrupts changed. The PCI-attached SATA controller
changed. The graphical interface and the ethernet also changed.

5:
The changes occured because these numbers will vary depending on the user.
If the user is more active on the system or less active, it will change the
number of times that these interrupt the system.

6:
I would imagine that these counts would indeed change depending on what software
is being run on the system. 

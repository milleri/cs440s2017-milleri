#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
//Compile on Linux: g++ -pthread helloWorldThread.c

pthread_t tid[2];

 void* task(void *procID)
 {
    unsigned long i = 0;
    long pid;
    pid = (long)procID;
    pthread_t id = pthread_self(); //pthread_self - obtain ID of the calling thread
    char processID;
    if(pid == 0){
      processID = 'A';
    }
    if(pid == 1){
      processID = 'B';
    }
    if(pid == 2){
      processID = 'C';
    }
// The pthread_equal() function returns a non-zero value if tread1 and thread2 are equal; otherwise, zero is returned.

    if(pthread_equal(id,tid[0]))
    {
        printf("\t\t+thread1 from Proc %c\n", processID);
    }
    else
    {
        printf("\t\t+thread2 from Proc %c\n", processID);
    }

    for(i=0; i<(0xFFFFFFFF);i++);

// For diagnostic purposes
//    {
//      printf("\n value of i =%lu",i);
//    }
    return NULL;
 }
 void *ProcA(){
   int i = 0;
   int err;
   long g = 0;
   printf("\tRunning: ProcA: \n");
//    while(i < 2)
   while(i < 2)
   {
       err = pthread_create(&(tid[i]), NULL, &task, (void *)g);
       if (err != 0)
           printf("\ncan't create thread :[%s]", strerror(err));

       i++;
   }

   sleep(5);
   return 0;
 }

 void *ProcB(){
   int i = 0;
   int err;
   long g = 1;
   printf("\tRunning: ProcB: \n");
//    while(i < 2)
   while(i < 2)
   {
       err = pthread_create(&(tid[i]), NULL, &task, (void *)g);
       if (err != 0)
           printf("\ncan't create thread :[%s]", strerror(err));

       i++;
   }

   sleep(5);
   return 0;
 }

 void *ProcC(){
   int i = 0;
   int err;
   long g = 2;
   printf("\tRunning: ProcC: \n");
//    while(i < 2)
   while(i < 2)
   {
       err = pthread_create(&(tid[i]), NULL, &task, (void *)g);
       if (err != 0)
           printf("\ncan't create thread :[%s]", strerror(err));

       i++;
   }

   sleep(5);
   return 0;
 }

 int main (int argc, char *argv[]){
   printf("Program Initialization \n");
   ProcA();
   ProcB();
   ProcC();
   printf("Program Termination \n");
   pthread_exit(NULL);
 }

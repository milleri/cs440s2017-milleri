#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include <semaphore.h>
//Compile on Linux: g++ -pthread helloWorldThread.c

pthread_t tid[2];
sem_t leftsem, rightsem;
pthread_t left_thread, right_thread;

 void* left(void *procID)
 {
    unsigned long i = 0;
    long pid;
    pid = (long)procID;
    pthread_t id = pthread_self(); //pthread_self - obtain ID of the calling thread
    char processID;
    if(pid == 0){
      processID = 'A';
    }
    if(pid == 1){
      processID = 'B';
    }
    if(pid == 2){
      processID = 'C';
    }
// The pthread_equal() function returns a non-zero value if tread1 and thread2 are equal; otherwise, zero is returned.


    sem_wait(&rightsem);
    printf("\t\t+thread1 from Proc %c\n", processID);
    sem_post(&leftsem);

 }
 void* right(void *procID)
 {
    unsigned long i = 0;
    long pid;
    pid = (long)procID;
    pthread_t id = pthread_self(); //pthread_self - obtain ID of the calling thread
    char processID;
    if(pid == 0){
      processID = 'A';
    }
    if(pid == 1){
      processID = 'B';
    }
    if(pid == 2){
      processID = 'C';
    }
// The pthread_equal() function returns a non-zero value if tread1 and thread2 are equal; otherwise, zero is returned.


    sem_wait(&leftsem);
    printf("\t\t+thread2 from Proc %c\n", processID);
    sem_post(&rightsem);
  }


 void *ProcA(){

  printf("\tRunning: ProcA: \n");
  long g = 0;
  pthread_create(&left_thread, NULL, left, (void *)g);
  pthread_create(&right_thread, NULL, right, (void *)g);
 	pthread_join(left_thread, NULL);
  pthread_join(right_thread, NULL);
 }

 void *ProcB(){
   printf("\tRunning: ProcB: \n");
   long g = 1;
   pthread_create(&left_thread, NULL, left, (void *)g);
   pthread_create(&right_thread, NULL, right, (void *)g);
   pthread_join(left_thread, NULL);
   pthread_join(right_thread, NULL);
 }

 void *ProcC(){
   printf("\tRunning: ProcC: \n");
   long g = 2;
   pthread_create(&left_thread, NULL, left, (void *)g);
   pthread_create(&right_thread, NULL, right, (void *)g);
   pthread_join(left_thread, NULL);
   pthread_join(right_thread, NULL);
 }

 int main (int argc, char *argv[]){
   sem_init(&leftsem, 0, 0);
   sem_init(&rightsem, 0, 1);
   printf("Program Initialization \n");
   ProcA();
   ProcB();
   ProcC();
   printf("Program Termination \n");
   pthread_exit(NULL);
 }

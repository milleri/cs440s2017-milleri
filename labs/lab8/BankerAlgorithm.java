/* Program Created for Lab 8
 * CS440s2017
 * Jacob Hanko and Izaak Miller
 *
 * To use: javac BankerAlgorithm.java
 java BankerAlgorithm
 */

import java.util.*;

public class BankerAlgorithm {

  public static void main(String[] args) {

    //scanner for the user input
    Scanner s = new Scanner(System.in);

    //global variables we need for the program
    int[] requiredValues = new int[4];
    int[] allocatedValues = new int[4];
    int[] needsValues = new int[4];
    int bankerFunds = 0;
    int availableFunds = 0;
    boolean done = true;

    System.out.println("Welcome to the Banker's Algorithm. \n");

    // enter the required values for the Borrowers
    System.out.println("Please enter the Required Values for the Borrowers: ");

    for (int i=0; i < requiredValues.length; i++) {
      requiredValues[i] = s.nextInt();
    }

    // enter the allocated values for the Borrowers
    System.out.println("Please enter the Allocated Values for the Borrowers: ");

    for (int i=0; i < allocatedValues.length; i++) {
      allocatedValues[i] = s.nextInt();
    }

    // enter the needs values for the Borrowers
    System.out.println("Please enter the Needs Values for the Borrowers: ");

    for (int i=0; i < needsValues.length; i++) {
      needsValues[i] = s.nextInt();
    }

    // enters the banker funds amount for the algorithm to work with
    System.out.println("Please enter Banker Funds amount: ");
    bankerFunds = s.nextInt();

    // creates the 4 borrowers, maybe not in the best way, but it was easy
    // for us to work with this way
    Borrower b1 = new Borrower("B1", requiredValues[0], allocatedValues[0], needsValues[0]);
    Borrower b2 = new Borrower("B2", requiredValues[1], allocatedValues[1], needsValues[1]);
    Borrower b3 = new Borrower("B3", requiredValues[2], allocatedValues[2], needsValues[2]);
    Borrower b4 = new Borrower("B4", requiredValues[3], allocatedValues[3], needsValues[3]);

    // while loop works through the algorithm we came up with until it reaches
    // either a stable or unstable state upon completion
    // Our algorithm is a Banker's Algorithm that focuses on taking the lowest
    // needed values first, simulating what we thought would work best
    while (done)
    {
      System.out.println();
      // calculates the available funds for the loop iteration
      availableFunds = bankerFunds - (b1.getBorrowerAllocatedValue() + b2.getBorrowerAllocatedValue() + b3.getBorrowerAllocatedValue() + b4.getBorrowerAllocatedValue());
      System.out.println("Current available funds: " + availableFunds);

      //sets a marker to the availableFunds so that the loop can located the lowest
      //needed value, thus lending to that borrower first
      int needMarker = availableFunds;

      for (int i=0; i < 4; i++)
      {
        if (needsValues[i] != 0 && needsValues[i] < needMarker)
        {
          needMarker = needsValues[i];
        }

      }

      // code that works through the calculations for Borrower 1, simulating
      // lending him/her money and gaining that money back
      if (b1.getBorrowerNeedsValue() == needMarker)
      {
        needsValues[0] = 0;
        System.out.println("B1 lowest needs, give B1 funds");

        b1.setBorrowerAllocatedValue(b1.getBorrowerAllocatedValue() + b1.getBorrowerNeedsValue());

        System.out.println("B1 new allocated funds: "+ b1.getBorrowerAllocatedValue());

        b1.setBorrowerNeedsValue(0);

        System.out.println("B1 now has 0 needs, required resources met.  Run B1.");

        b1.setBorrowerAllocatedValue(0);

        availableFunds += b1.getBorrowerRequiredValue();

        b1.setBorrowerRequiredValue(0);

        System.out.println("B1 Process complete. Searching for new lowest need. ");

      }

      // code that works through the calculations for Borrower 2, simulating
      // lending him/her money and gaining that money back
      else if (b2.getBorrowerNeedsValue() == needMarker)
      {
        needsValues[1] = 0;
        System.out.println("B2 lowest needs, give B2 funds");

        b2.setBorrowerAllocatedValue(b2.getBorrowerAllocatedValue() + b2.getBorrowerNeedsValue());

        System.out.println("B2 new allocated funds: "+ b2.getBorrowerAllocatedValue());

        b2.setBorrowerNeedsValue(0);

        System.out.println("B2 now has 0 needs, required resources met.  Run B2.");

        b2.setBorrowerAllocatedValue(0);

        availableFunds += b2.getBorrowerRequiredValue();

        b2.setBorrowerRequiredValue(0);

        System.out.println("B12 Process complete. Searching for new lowest need. ");
      }

      // code that works through the calculations for Borrower 3, simulating
      // lending him/her money and gaining that money back
      else if (b3.getBorrowerNeedsValue() == needMarker)
      {
        needsValues[2] = 0;
        System.out.println("B3 lowest needs, give B3 funds");

        b3.setBorrowerAllocatedValue(b3.getBorrowerAllocatedValue() + b3.getBorrowerNeedsValue());

        System.out.println("B3 new allocated funds: "+ b3.getBorrowerAllocatedValue());

        b3.setBorrowerNeedsValue(0);

        System.out.println("B3 now has 0 needs, required resources met.  Run B3.");

        b3.setBorrowerAllocatedValue(0);

        availableFunds += b3.getBorrowerRequiredValue();

        b3.setBorrowerRequiredValue(0);

        System.out.println("B3 Process complete. Searching for new lowest need. ");
      }

      // code that works through the calculations for Borrower 4, simulating
      // lending him/her money and gaining that money back
      else if (b4.getBorrowerNeedsValue() == needMarker)
      {
        needsValues[3] = 0;
        System.out.println("B4 lowest needs, give B4 funds");

        b4.setBorrowerAllocatedValue(b4.getBorrowerAllocatedValue() + b4.getBorrowerNeedsValue());

        System.out.println("B4 new allocated funds: "+ b4.getBorrowerAllocatedValue());

        b4.setBorrowerNeedsValue(0);

        System.out.println("B4 now has 0 needs, required resources met.  Run B4.");

        b4.setBorrowerAllocatedValue(0);

        availableFunds += b4.getBorrowerRequiredValue();

        b4.setBorrowerRequiredValue(0);

        System.out.println("B4 Process complete. Searching for new lowest need. ");
      }
      //end result, changes the boolean to false, stopping the loop
      else if (availableFunds == bankerFunds){
        System.out.println("The borrowers were all given the appropriate resources in the appropriate order. The banker's alogorithm is now complete.");
        done = false;
      }
      //else that catches unstable state runs
      else{
        System.out.println("Unable to distribute resources...");
        System.out.println("Unstable-state run...");
        System.out.println("Deadlock occured!");
        done = false;
      }
    }

  }//public static ...
}

// our borrower class where stored object information inputted by the user
// allowing easy and managable variables for each borrower object Created
// below are the get and set methods for the variables within the object
class Borrower{
  String name;
  int requiredValue;
  int allocatedValue;
  int needsValue;

  public Borrower (String n, int r, int a, int v)
  {
     name = n;
     requiredValue = r;
     allocatedValue = a;
     needsValue = v;
  }

  public void setBorrowerName(String n)
  {
      name = n;
  }

  public void setBorrowerRequiredValue(int rv)
  {
    requiredValue = rv;
  }

  public void setBorrowerAllocatedValue(int av)
  {
    allocatedValue = av;
  }

  public void setBorrowerNeedsValue(int nv)
  {
    needsValue = nv;
  }

  public String getBorrowerName()
  {
      return name;
  }

  public int getBorrowerRequiredValue()
  {
    return requiredValue;
  }

  public int getBorrowerAllocatedValue()
  {
    return allocatedValue;
  }

  public int getBorrowerNeedsValue()
  {
    return needsValue;
  }
}
